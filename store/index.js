import { combineReducers } from 'redux'; // 3.7.2
import StoryReducer from './StoryReducer';
import SelectionReducer from './SelectionReducer';

export default combineReducers({
  stories: StoryReducer,
  selectedStoryId: SelectionReducer
});
