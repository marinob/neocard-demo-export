module.exports = {
  APP_BACKGROUND_COLOR: '#303030',
  DARK_BACKGROUND_1: '#000',
  DARK_BACKGROUND_2: '#202020',
  DARK_BACKGROUND_3: '#303030',
  LIGHT_COLOR_1: '#D9B4A4',
  LIGHT_COLOR_2: '#FFFFFF',
  FONT: 'Apple SD Gothic Neo', // available by default on iOS only, can be added to Android as custom font
  BANKS: [
      ['hanzaee', 'Swedbank Eesti'],
      ['sebee', 'SEB Pank Eesti'],
      ['fi_pohjola', 'Op-Pohjola Suomi'],
      ['fi_s_pankki', 'S-Pankki Suomi'],
    ],
  TEXTS: [
      'Maksa 1.00€', // 0
      'Pangalingi valik', // 1
      'Osta', // 2
      'Tühista', // 3
  ],
  PLACEHOLD_EXIT: 'http://via.placeholder.com/300?text=' // intend to redirect to payment provider
};