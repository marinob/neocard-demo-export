import React from 'react';
import { Text, TouchableOpacity } from 'react-native';

const GLOBAL = require('../Globals');

const Button = ({ onPress, children }) => {
  const { buttonStyle, textStyle } = styles;

  return (
    <TouchableOpacity onPress={onPress} style={buttonStyle}>
      <Text style={textStyle}>
        {children}
      </Text>
    </TouchableOpacity>
  );
};

const styles = {
  textStyle: {
    alignSelf: 'center',
    color: GLOBAL.LIGHT_COLOR_2,
    fontFamily: GLOBAL.FONT,
    fontSize: 16,
    fontWeight: '600',
    paddingBottom: 10,
    paddingTop: 10,
  },
  buttonStyle: {
    alignSelf: 'stretch',
    backgroundColor: GLOBAL.DARK_BACKGROUND_3,
    borderColor: GLOBAL.LIGHT_COLOR_1,
    borderRadius: 5,
    borderWidth: 1,
    flex: 1,
    marginLeft: 5,
    marginRight: 5
  }
};

export { Button };
