import React, { Component } from 'react';
import {
  Text,
  TouchableWithoutFeedback,
  View,
  LayoutAnimation,
  Image,
  Alert,
  Linking
} from 'react-native';
import { connect } from 'react-redux'; // 5.0.6

import { StoryViewArea } from './StoryViewArea';
import { Button } from './Button';

import * as actions from '../actions';

import "redux";  // 3.7.2

const GLOBAL = require('../Globals');

const mapStateToProps = (state, ownProps) => {
  const expanded = state.selectedStoryId === ownProps.story.id;
  return { expanded };
};

class StoryViewCard extends Component {
  
  componentWillUpdate() {
    LayoutAnimation.spring();
  }

  renderDescription() {
    const { story, expanded } = this.props;

    // converting banks into alert instructors
    var bankActions = [];
    GLOBAL.BANKS.forEach(function(item){
        bankActions.push({text: item[1], onPress: () => Linking.openURL(GLOBAL.PLACEHOLD_EXIT+item[0])});
    });
    bankActions.push({text: GLOBAL.TEXTS[3], onPress: () => [], style: 'cancel'});

    if (expanded) {
      return (
        <View>
          <StoryViewArea>
            <Text style={styles.contentStyles}>
              {story.message}
            </Text>
          </StoryViewArea>
          <StoryViewArea>
            <Button onPress={() => Alert.alert(GLOBAL.TEXTS[0], GLOBAL.TEXTS[1], bankActions, { cancelable: false })}>
              {GLOBAL.TEXTS[2]}
            </Button>
          </StoryViewArea>
          <StoryViewArea>
            <Image style={styles.bgContainer} source={{ uri: story.fullPicture }}  />
          </StoryViewArea>
        </View>
      );
    }
  }

  render() {
    const { titleStyle } = styles;
    const { id, messageTags } = this.props.story;

    return (
      <TouchableWithoutFeedback onPress={() => this.props.selectStory(id)}>
        <View style={styles.cardStyle}>
          <StoryViewArea darkBackground='true'>
            <Text style={titleStyle}>
              {messageTags}
            </Text>
          </StoryViewArea>
          {this.renderDescription()}
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

const styles = {
  cardStyle: {
    borderBottomWidth: 1,
    borderColor: GLOBAL.DARK_BACKGROUND_1
  },
  titleStyle: {
    color: GLOBAL.LIGHT_COLOR_1,
    fontFamily: GLOBAL.FONT,
    fontSize: 20,
    paddingBottom: 10,
    paddingLeft: 15,
    paddingTop: 10,
  },
  contentStyles: {
    color: GLOBAL.LIGHT_COLOR_1,
    flex: 1,
    fontFamily: GLOBAL.FONT,
    paddingLeft: 15,
    paddingRight: 15,
  },
  descriptionStyle: {
    paddingLeft: 15,
    paddingRight: 15
  },
  bgContainer: {
    flex: 1,
    width: 100,
    height: 200
  }
};

export default connect(mapStateToProps, actions)(StoryViewCard);