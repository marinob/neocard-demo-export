import React, { Component } from 'react';
import { ListView } from 'react-native';
import { connect } from 'react-redux'; // 5.0.6

import StoryViewCard from './StoryViewCard';

import "redux"; // 3.7.2

class StoryList extends Component {
  
  componentWillMount() {
    const ds = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2
    });
    this.dataSource = ds.cloneWithRows(this.props.stories);
  }

  renderRow(story) {
    return <StoryViewCard story={story} />;
  }

  render() {
    return (
      <ListView
        dataSource={this.dataSource}
        renderRow={this.renderRow}
      />
    );
  }

}

const mapStateToProps = state => {
  return { stories: state.stories };
};

export default connect(mapStateToProps)(StoryList);