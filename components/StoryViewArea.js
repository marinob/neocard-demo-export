import React from 'react';
import { View } from 'react-native';

const GLOBAL = require('../Globals');

const styles = {
  containerStyle: {
    borderBottomWidth: 0,
    padding: 5,
    justifyContent: 'flex-start',
    flexDirection: 'row',
    borderColor: GLOBAL.DARK_BACKGROUND_3,
    position: 'relative'
  }
};

const StoryViewArea = (props) => {

  styles.containerStyle.backgroundColor = props.darkBackground ? GLOBAL.DARK_BACKGROUND_3 : GLOBAL.DARK_BACKGROUND_2;

  return (
    <View style={styles.containerStyle}>
      {props.children}
    </View>
  );
};



export { StoryViewArea };
