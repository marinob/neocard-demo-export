import React from 'react';
import { View, Image } from 'react-native';

const GLOBAL = require('../Globals');

const Header = (props) => {

  return (
    <View style={styles.viewStyle}>
      <Image
        source={props.headerImage}
        style={styles.headerImageStyle}
      />
      {/* untested for hd/retina displays */}
    </View>
  );
};

const styles = {
  viewStyle: {
    backgroundColor: GLOBAL.DARK_BACKGROUND_1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 15,
    paddingBottom: 15,
    shadowColor: '#fff',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.2,
    elevation: 2,
    position: 'relative'
  },
  headerImageStyle: {
    resizeMode: 'contain',
    width: 300,
    height: 40
  }
};

export { Header };
