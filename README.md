# Neocard Demo

This is a small demo prepared for my job interview with React. The aim for this demo is to prepare an essentially basic demo app that includes usage of React, React Native and Redux. It was prepared over half working day just to give a basic concept.

The demo includes from Stephen Grinder's useful tutorial **The Complete React Native and Redux course**. My idea was to redesign the app with the design and the color of scheme of Tartu's Kvartal shopping mall, including stories from the mall, adding images, and add some extra interaction.
There was an attempt to add a simple payment redirection button which has been replaced with a placeholder due to lack of time.

## Features

At loading time, the app loads a (now cached and embedded) list of stories imported from Kvartal facebook page. The requirement for a story to be eligible were:
* Have a min of 20 characters and a max of 600 characters
* Include a picture
* Have at least a tag to another page (e.g. one of the shop stores)

The processing of this information was done with a simple PHP backend that was fetching the public data of the page from the Facebook Graph API, do a light processing and returning to the app. To avoid dependency form the backend, in this demo the data have been already processed and stored offline (the story images will be loaded upon opening the application.

The stories are listed in a dynamic list of cards, where the status of the active card is stored with Redux. Upon pressing one of the card, the state is update and the new active card (if any) is shown on the foreground together with the story info, the purchase button, and the image.

A button press will now simply redirect the page to an alert screen where users can choose the preferred payment method or (now added) the possibility to close the alert. Screen will be then redirected to a placeholder page which will anyway show the label related to the selected payment method.

## Extra Info

* Colors, font, and static text labels were stored in a global component for easy access and configuration
* For styling purposes and simplicity, a iOS only font was used. On Android phones this font shall be embedded in the app.
