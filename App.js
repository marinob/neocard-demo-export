// npm imports
import React, {Component} from 'react';
import { SafeAreaView } from 'react-native';
import { Provider } from 'react-redux';
import { createStore } from 'redux';

// src imports
import reducers from './store';
import StoryList from './components/StoryList';
import { Header } from './components/Header';

const GLOBAL = require('./Globals');

class App extends Component {

  render() {
    return (
      <Provider store={createStore(reducers)}>
        <SafeAreaView style={styles.safeAreaViewStyle}>{/*  Prevents overriding of iOS/Android status bar */}
          <Header headerImage={require('./static/headlineTransparent.png')} /> {/* Header with Kvartal logo */}
          <StoryList />{/*  List of news */}
        </SafeAreaView>
      </Provider>
    );
  }

}

const styles = {
  safeAreaViewStyle: {
    flex: 1,
    backgroundColor: GLOBAL.APP_BACKGROUND_COLOR  
  }
};

export default App;
